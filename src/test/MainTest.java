package test;

import main.Main;
import main.Transaction;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class MainTest {

    @Test
    public void testJobs() {
        int numberOfJobs  = 10;
        List<Transaction> jobs =  Main.createJobs(numberOfJobs);
        Assert.assertEquals(jobs.size(), 10);
    }
}
