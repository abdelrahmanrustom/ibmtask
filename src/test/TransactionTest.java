package test;

import junit.framework.TestCase;
import main.Transaction;
import org.junit.Assert;
import org.junit.Test;

public class TransactionTest extends TestCase {

    @Test
    public void testTranscationNumber() {
        Transaction transaction = new Transaction(5);
        Assert.assertEquals(transaction.getTransactionNumber(), 5);
    }

    @Test
    public void testExecuteInTransaction(){
        try {
            Assert.assertEquals(Transaction.class.getMethod("execute", null).getReturnType().getName(),"void");
        } catch (NoSuchMethodException e) {
            fail();
            e.printStackTrace();
        }
    }

    @Test
    public void testClassesImplementedInTransaction(){
           Class classes[] = Transaction.class.getInterfaces();
            int count = 0;
            for (int i = 0; i< classes.length; i ++ ){
                if (classes[i].getSimpleName().equals("Runnable") || classes[i].getSimpleName().equals("IJob")){
                    count ++;
                }
            }
            Assert.assertEquals(count, 2);
    }

}
