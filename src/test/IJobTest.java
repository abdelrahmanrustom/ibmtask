package test;

import main.IJob;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.fail;

public class IJobTest {

    @Test
    public void testExecuteInTransaction(){
        try {
            Assert.assertEquals(IJob.class.getMethod("execute", null).getReturnType().getName(),"void");
        } catch (NoSuchMethodException e) {
            fail();
            e.printStackTrace();
        }
    }
}
