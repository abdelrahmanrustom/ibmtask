package main;

public class Transaction implements Runnable, IJob {

    private int transactionNumber;

    public Transaction (int number) {
        this.transactionNumber = number;
    }

    @Override
    public void run() {
        this.execute();
    }

    @Override
    public void execute() {
        //Do stuff
        System.out.println("Transaction number: " + transactionNumber + " is in execution");
    }

    public int getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(int transactionNumber) {
        this.transactionNumber = transactionNumber;
    }
}
