package main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static void main(String[]args) {
        int numberOfJobs = 10000;
        int numberOfThreads =2;
        List<Transaction> jobs = createJobs(numberOfJobs);
        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
        executeJobs(jobs, executor);
    }

    public static List<Transaction> createJobs(int numberOfJobs){
        Transaction transaction;
        List<Transaction> allJobs = new ArrayList<>();
        for (int i = 1; i <= numberOfJobs; i ++){
            transaction = new Transaction(i);
            allJobs.add(transaction);
        }
        return allJobs;
    }

    public static void executeJobs(List<Transaction> jobs, ExecutorService executor){
        Future item;
        for (int i = 0; i < jobs.size(); i++){
            item = executor.submit(jobs.get(i));
            try {
                if (item.get() != null){
                    executor.shutdownNow();
                } else {
                    System.out.println("Transaction number: " + (i+1) + " is done");
                }
            } catch (InterruptedException e) {
                executor.shutdownNow();
                e.printStackTrace();
            } catch (ExecutionException e) {
                executor.shutdownNow();
                e.printStackTrace();
            }
        }
    }
}